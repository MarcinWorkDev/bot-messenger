﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bot.Logic.Models;
using Bot.Logic.Data;
using Microsoft.EntityFrameworkCore;

namespace Bot.Logic.Services
{
    public class BotResultRepository : IBotResultRepository
    {
        private readonly BOTMsgDataContext _context;

        public BotResultRepository(BOTMsgDataContext context)
        {
            _context = context;
        }

        public void Add(BotResult botResult, string botInteractionId)
        {
            BotInteraction interaction = _context.BotInteraction.Where(w => w.BotInteractionId == Guid.Parse(botInteractionId)).FirstOrDefault();
            botResult.Interaction = interaction;

            _context.BotResult.Add(botResult);
            _context.SaveChanges();
        }

        public void Cancel(string TokenGuidString)
        {
            BotInteraction botInteraction = _context.BotInteraction.Where(w => w.BotInteractionId == Guid.Parse(TokenGuidString)).FirstOrDefault();
            if (botInteraction != null)
            {
                botInteraction.SessionEnd = DateTime.Now;
                _context.BotInteraction.Update(botInteraction);
                _context.SaveChanges();
            }
        }

        public IQueryable<BotResult> GetAll()
        {
            return _context.BotResult.Include("Interaction");
        }

        public BotResult GetOne(int resultId)
        {
            return _context.BotResult.Where(x => x.ResultId == resultId).FirstOrDefault();
        }

        public BotInteraction NewInteraction(string MessengerUserId, string BotUserId)
        {
            BotInteraction interaction = new BotInteraction
            {
                SessionStart = DateTime.Now,
                MessengerUserId = MessengerUserId,
                BotUserId = BotUserId
            };
            _context.BotInteraction.Add(interaction);
            _context.SaveChanges();
            return interaction;
        }
    }
}
