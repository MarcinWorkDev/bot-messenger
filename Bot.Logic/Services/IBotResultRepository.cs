﻿using System;
using System.Collections.Generic;
using System.Text;
using Bot.Logic.Models;
using System.Linq;

namespace Bot.Logic.Services
{
    public interface IBotResultRepository
    {
        BotResult GetOne(int resultId);
        IQueryable<BotResult> GetAll();
        void Add(BotResult botResult, string BotInteractionId);
        BotInteraction NewInteraction(string MessengerUserId, string BotUserId);
        void Cancel(string TokenGuidString);
    }
}
