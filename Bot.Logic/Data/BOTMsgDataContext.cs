﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Bot.Logic.Models;

namespace Bot.Logic.Data
{
    public class BOTMsgDataContext : DbContext
    {
        public BOTMsgDataContext(DbContextOptions<BOTMsgDataContext> options) : base (options)
        {

        }

        public DbSet<BotResult> BotResult { get; set; }
        public DbSet<BotInteraction> BotInteraction { get; set; }
    }
}
