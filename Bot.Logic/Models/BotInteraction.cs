﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bot.Logic.Models
{
    public class BotInteraction
    {
        [Key]
        public Guid BotInteractionId { get; set; }
        
        public DateTime? SessionStart { get; set; }

        public DateTime? SessionEnd { get; set; }

        public string MessengerUserId { get; set; }

        public string BotUserId { get; set; }
    }
}
