﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bot.Logic.Models
{
    public class BotResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ResultId { get; set; }

        //public Guid BotInteractionId { get; set; }

        [ForeignKey("BotInteractionId")]
        public BotInteraction Interaction { get; set; }

        [Required]
        public DateTime ResultTimestamp { get; set; }

        public string ResultString { get; set; }

        public string ResCustomerFirstName { get; set; }

        public string ResCustomerLastName { get; set; }

        public string ResFirstName { get; set; }

        public string ResLastName { get; set; }

        public string ResEmail { get; set; }

        public string ResPhone { get; set; }
    }
}
