﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Bot.Webview.Controllers
{
    [Route("/Bot/[Action]")]
    public class BotController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("",Name ="ContactForm")]
        public IActionResult ContactForm()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ContactFormApi()
        {
            JObject json = new JObject()
            {
                ["messages"] = new JObject()
                {
                    ["attachment"] = new JObject()
                    {
                        ["type"] = "template",
                        ["payload"] = new JObject()
                        {
                            ["template_type"] = "generic",
                            ["elements"] = new JArray()
                            {
                                new JObject()
                                {
                                    ["title"] = "Formularz Kontaktowy",
                                    ["default_action"] = new JObject()
                                    {
                                        ["type"] = "web_url",
                                        ["url"] = Url.Link("ContactForm", new { }),
                                        ["messenger_extensions"] = true
                                    },
                                    ["buttons"] = new JArray()
                                    {
                                        new JObject()
                                        {
                                            ["type"] = "web_url",
                                            ["url"] = Url.Link("ContactForm", new { }),
                                            ["title"] = "Formularz kontaktowy"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            return Ok(json);
        }
    }
}