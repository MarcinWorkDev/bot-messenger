﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.Webservice.Models
{
    public class BotJsonText
    {
        public BotJsonText(string text)
        {
            this.text = text;
        }

        public string text { get; set; }
    }
}
