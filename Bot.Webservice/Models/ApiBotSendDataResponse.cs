﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.Webservice.Models
{
    public class ApiBotSendDataResponse
    {
        public ApiBotSendDataResponse()
        {
            messages = new List<BotJsonText>();
        }

        public List<BotJsonText> messages { get; set; }
    }
}
