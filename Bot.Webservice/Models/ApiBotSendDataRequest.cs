﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.Webservice.Models
{
    public class ApiBotSendDataRequest
    {
        public string token_api { get; set; }
        public string token_guid { get; set; }
        public string customer_first_name { get; set; }
        public string customer_last_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
    }
}
