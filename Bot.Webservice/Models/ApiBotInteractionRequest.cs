﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.Webservice.Models
{
    public class ApiBotInteractionRequest
    {
        public string token_api { get; set; }
        public string token_guid { get; set; }
        public string messenger_user_id { get; set; }
        public string chatfuel_user_id { get; set; }
    }
}
