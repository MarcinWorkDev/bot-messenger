﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bot.Webservice.Models
{
    public class ApiBotInteractionResponse
    {
        public ApiBotInteractionResponse()
        {
            messages = new List<BotJsonText>();
            set_attributes = new BotJsonSetAttributes()
            {
                token_guid = "empty"
            };
        }

        public BotJsonSetAttributes set_attributes;
        public List<BotJsonText> messages;
    }
}
