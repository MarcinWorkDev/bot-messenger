﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Bot.Logic.Services;
using Bot.Logic.Models;
using Bot.Webservice.Models;

namespace Bot.Webservice.Controllers
{
    [Produces("application/json")]
    [Route("api/Bot/[action]")]
    public class BotController : Controller
    {
        private readonly IBotResultRepository _repository;

        private static string TOKEN_API = "dfrthersfvwervge2fwf3223";

        public BotController(IBotResultRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public IActionResult OpenSession(ApiBotInteractionRequest apiBotInteraction)
        {
            ApiBotInteractionResponse response = new ApiBotInteractionResponse();
            try
            {
                if (apiBotInteraction == null)
                {
                    response.messages.Add(new BotJsonText(String.Format("Dodany guid: {0}", "request is null")));
                    return Ok(response);
                }

                if (apiBotInteraction.token_api == null || apiBotInteraction.token_api != TOKEN_API)
                {
                    response.messages.Add(new BotJsonText(String.Format("Dodany guid: {0}", "token is null or invalid")));
                    return Ok(response);
                }
                
                BotInteraction botInteraction = _repository.NewInteraction(apiBotInteraction.messenger_user_id, apiBotInteraction.chatfuel_user_id);
                response.set_attributes.token_guid = botInteraction.BotInteractionId.ToString();
                response.messages.Add(new BotJsonText(String.Format("Dodany guid: {0}", botInteraction.BotInteractionId.ToString())));
                return Ok(response);
            } catch (Exception ex)
            {
                response.messages.Add(new BotJsonText(ex.Message));
                return Ok(response);
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            IQueryable<BotResult> bots = _repository.GetAll();

            if (bots.Count() == 0)
            {
                return NotFound();
            }

            return Ok(bots);
        }
        
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            BotResult bot = _repository.GetOne(id);

            if (bot == default(BotResult))
            {
                return NotFound();
            }

            return Ok(bot);
        }
        
        [HttpPost]
        public IActionResult Add(ApiBotSendDataRequest request)
        {
            ApiBotSendDataResponse response = new ApiBotSendDataResponse();

            try
            {
                
                if (request == null || request.token_api == null || request.token_api != TOKEN_API)
                {
                    response.messages.Add(new BotJsonText("Błędny token!"));
                    return Ok(response);
                }

                BotResult bot = new BotResult()
                {
                    ResultTimestamp = DateTime.Now,
                    ResCustomerFirstName = request.customer_first_name,
                    ResCustomerLastName = request.customer_last_name,
                    ResFirstName = request.first_name,
                    ResLastName = request.last_name,
                    ResEmail = request.email,
                    ResPhone = request.phone_number
                };

                _repository.Add(bot, request.token_guid);
                response.messages.Add(new BotJsonText("Dane zapisane w bazie!"));

                return Ok(response);
            }
            catch (Exception ex)
            {
                response.messages.Add(new BotJsonText(String.Format("Błąd: {0} {1}", ex.Message, ex.InnerException.Message)));
                return Ok(response);
            }
        }

        [HttpPost]
        public IActionResult CloseSession(ApiBotInteractionRequest request)
        {
            ApiBotInteractionResponse response = new ApiBotInteractionResponse();

            if (request == null)
            {
                response.messages.Add(new BotJsonText(String.Format("Error: ", "request is null")));
                return Ok(response);
            }

            if (request.token_api == null || request.token_api != TOKEN_API)
            {
                response.messages.Add(new BotJsonText(String.Format("Error: ", "token is null or invalid")));
                return Ok(response);
            }

            if (request.token_guid == null)
            {
                response.messages.Add(new BotJsonText(String.Format("Error: ", "guid is null")));
                return Ok(response);
            }

            _repository.Cancel(request.token_guid);
            return Ok();
        }

    }
}
